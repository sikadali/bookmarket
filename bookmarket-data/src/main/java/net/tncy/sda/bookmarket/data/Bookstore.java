package net.tncy.sda.bookmarket.data;

public class Bookstore {
    private int id;
    private String name;
    private InventoryEntry[] inventoryEntries;

    public Bookstore(int id, String name) {
        this.id = id;
        this.name = name;
    }

    // GETTERS AND SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InventoryEntry[] getInventoryEntries() {
        return inventoryEntries;
    }

    public void setInventoryEntries(InventoryEntry[] inventoryEntries) {
        this.inventoryEntries = inventoryEntries;
    }
}
