package net.tncy.sda.bookmarket.data;

import net.tncy.sda.validation.constraints.ISBN;

//VERSION 1.0.11 RELEASE
public class Book {
    public enum BookFormat{
        BROCHE, POCHE
    }
    private int id;
    private String title;
    private String author;
    private String publisher;
    private BookFormat format;
    @ISBN
    private String isbn;

    public Book(int id, String title, String author, String publisher, BookFormat format, String isbn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }

    // GETTERS AND SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BookFormat getFormat() {
        return format;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
