package net.tncy.sda.bookmarket.services;

import net.tncy.sda.bookmarket.data.Bookstore;
import net.tncy.sda.bookmarket.data.InventoryEntry;

public class BookstoreService {
    public int createBookstore(String name){
        return 0;
    }
    public void deleteBookstore(int bookstoreId){}
    public Bookstore getBookstoreById(int bookstoreId){
        return null;
    }
    public Bookstore[] findBookstoreByName(String name){
        return null;
    }
    public void renameBookstore(int bookstoreId, String newName){}
    public void addBookToBookstore(int bookstoreId, int bookId, int qty, float price){}
    public float removeBookFromBookstore(int bookstoreId, int bookId, int qty){
        return 0;
    }
    public InventoryEntry[] getBookstoreInventory(int bookstoreId){
        return null;
    }
    //public getBookstoreCatalog(int bookstoreId){}
    public Bookstore[] getAllBookstores(){
        return null;
    }
}
